package com.example.barber.repositories;

import com.example.barber.entities.Barber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarberRepository extends JpaRepository<Barber, Long> {

    @Query("SELECT b FROM Barber b where b.location.city = ?1")
    List<Barber> getBarbersByCity(String city);
}
