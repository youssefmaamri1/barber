package com.example.barber.controllers;

import com.example.barber.entities.Location;
import com.example.barber.loggers.Loggers;
import com.example.barber.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @PostMapping("/location")
    private ResponseEntity<Location> createLocation(@RequestBody Location location) {
        try {
            Location newLocation = locationService.createLocation(location);
            Loggers.LOGGER_INFO.info("successfully created location !");
            return new ResponseEntity<>(newLocation, HttpStatus.CREATED);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/locations")
    private ResponseEntity<List<Location>> getAllLocations() {
        try {
            List<Location> locations = locationService.getAllLocations();
            Loggers.LOGGER_INFO.info("successfully retrieving locations !");
            return new ResponseEntity<>(locations, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/locations/cities")
    private ResponseEntity<List<String>> getCities() {
        try {
            List<String> cities = locationService.getAllLocations().stream().map(
                    Location :: getCity).collect(Collectors.toList());
            return new ResponseEntity<>(cities.stream().sorted().collect(Collectors.toList()), HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
