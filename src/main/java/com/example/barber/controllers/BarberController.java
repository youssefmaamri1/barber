package com.example.barber.controllers;

import com.example.barber.entities.Location;
import com.example.barber.loggers.Loggers;
import com.example.barber.entities.Barber;
import com.example.barber.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.barber.service.BarberService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class BarberController {

    @Autowired
    private BarberService barberService;
    @Autowired
    private LocationService locationService;

    @PostMapping("/barber")
    private ResponseEntity<Barber> createBarber(@RequestBody Barber barber) {
        try {
            Location location = locationService.createLocation(barber.getLocation());
            if(location != null) {
                barber.setLocation(location);
                Barber createdBarber = barberService.createBarber(barber);
                Loggers.LOGGER_INFO.info("successfully created barber !");
                return new ResponseEntity<>(createdBarber, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch(Exception ex) {
            Loggers.LOGGER_INFO.info("error when creating barber with cause : {} ", ex.getCause());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/barber/{id}")
    private ResponseEntity<Barber> getBarber(@PathVariable("id") long barberId) {
        try {
            Optional<Barber> barber = barberService.getBarber(barberId);
            if(barber.isPresent()) {
                Loggers.LOGGER_INFO.info("barber found !");
                return new ResponseEntity<>(barber.get(), HttpStatus.OK);
            } else {
                Loggers.LOGGER_INFO.info("barber not found !");
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            Loggers.LOGGER_INFO.info("error when retrieving barber with cause : {} ", ex.getCause());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/barbers")
    private ResponseEntity<List<Barber>> getAllBarbers() {
        try {
            List<Barber> barbers = barberService.getAllBarbers();
            Loggers.LOGGER_INFO.info("barbers found ");
            return new ResponseEntity<>(barbers, HttpStatus.OK);
        } catch(Exception ex) {
            Loggers.LOGGER_INFO.info("error when searching for barbers with cause : {} ", ex.getCause());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/barbers/barbersByCity")
    private ResponseEntity<List<Barber>> getBarbersByCity(@RequestParam(required = true) String city) {
        try {
            List<Barber> barbers = barberService.getBarbersByCity(city);
            Loggers.LOGGER_INFO.info("barbers found for city : {} ", city);
            return new ResponseEntity<>(barbers, HttpStatus.OK);
        } catch(Exception ex) {
            Loggers.LOGGER_INFO.info("error when searching for barbers with cause : {} ", ex.getCause());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/barber/{id}")
    private ResponseEntity<Barber> updateBarber(@PathVariable("id") long barberId, @RequestBody Barber newBarber) {
        try {
            Optional<Barber> barber = barberService.getBarber(barberId);
            if(barber.isPresent()) {
                newBarber.getLocation().setLocationId(barber.get().getLocation().getLocationId());
                barber.get().setLocation(newBarber.getLocation());
                barber.get().setAddress(newBarber.getAddress());
                barber.get().setEmail(newBarber.getEmail());
                barber.get().setFirstName(newBarber.getFirstName());
                barber.get().setLastName(newBarber.getLastName());
                barber.get().setPhone(newBarber.getPhone());
                barberService.createBarber(barber.get());
                Loggers.LOGGER_INFO.info("barber successfully updated !");
                return new ResponseEntity<>(barber.get(), HttpStatus.OK);
            } else {
                Loggers.LOGGER_INFO.info("barber not found : {} ", barberId);
                return new ResponseEntity<>(barber.get(), HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            Loggers.LOGGER_INFO.info("error when updating barber with cause : {} ", ex.getCause());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/barber/{id}")
    private ResponseEntity deleteBarber(@PathVariable("id") long barberId) {
        try {
            barberService.deleteBarber(barberId);
            return new ResponseEntity(null, HttpStatus.OK);
        } catch(Exception ex) {
            Loggers.LOGGER_INFO.info("error when deleting barber with cause : {} ", ex.getCause());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
