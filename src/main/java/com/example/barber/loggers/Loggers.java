package com.example.barber.loggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class Loggers {
    private Loggers() {

    }
    public static final String LOGGER_NAME = "LOGGER";
    public static final Logger LOGGER_INFO = LoggerFactory.getLogger(LOGGER_NAME);

}
