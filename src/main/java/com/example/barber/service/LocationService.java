package com.example.barber.service;

import com.example.barber.entities.Location;

import java.util.List;
import java.util.Optional;

public interface LocationService {
    Location createLocation(Location location);
    Optional<Location> getLocation(long locationId);
    List<Location> getAllLocations();
}
