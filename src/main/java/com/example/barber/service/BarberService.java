package com.example.barber.service;

import com.example.barber.entities.Barber;

import java.util.List;
import java.util.Optional;

public interface BarberService {
    Barber createBarber(Barber barber);
    Optional<Barber> getBarber(long barberId);
    List<Barber> getAllBarbers();
    List<Barber> getBarbersByCity(String city);
    void deleteBarber(long barberId);
}
