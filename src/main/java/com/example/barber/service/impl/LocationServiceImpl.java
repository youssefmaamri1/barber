package com.example.barber.service.impl;

import com.example.barber.entities.Location;
import com.example.barber.repositories.LocationRepository;
import com.example.barber.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Location createLocation(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public Optional<Location> getLocation(long locationId) {
        return Optional.empty();
    }

    @Override
    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }
}
