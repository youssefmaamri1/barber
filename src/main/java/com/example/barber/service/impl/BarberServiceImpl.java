package com.example.barber.service.impl;

import com.example.barber.entities.Barber;
import com.example.barber.service.BarberService;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Service;
import com.example.barber.repositories.BarberRepository;

import java.util.List;
import java.util.Optional;

@Service
public class BarberServiceImpl implements BarberService {
    @Autowired
    private BarberRepository barberRepository;

    @Override
    public Barber createBarber(Barber barber) {
        return barberRepository.save(barber);
    }

    @Override
    public Optional<Barber> getBarber(long barberId) {
        return barberRepository.findById(barberId);
    }

    @Override
    public List<Barber> getAllBarbers() {
        return barberRepository.findAll();
    }

    @Override
    public List<Barber> getBarbersByCity(String city) {
     //   return barberRepository.getBarbersByCity(city);
        return null;
    }

    @Override
    public void deleteBarber(long barberId) {
        barberRepository.deleteById(barberId);
    }
}
